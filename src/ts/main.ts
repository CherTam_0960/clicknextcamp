// const divApp: HTMLDivElement = document.getElementById("app") as HTMLDivElement;
// divApp.innerText = "Hello TypeScript";

const inputNumber = document.getElementById("inputNumber") as HTMLInputElement;
const equalButton = document.getElementById("equalButton") as HTMLButtonElement;
const notEqualButton = document.getElementById(
  "notEqualButton"
) as HTMLButtonElement;
const moreThanButton = document.getElementById(
  "moreThanButton"
) as HTMLButtonElement;
const moreThanOrEqualButton = document.getElementById(
  "moreThanOrEqualButton"
) as HTMLButtonElement;
const output = document.getElementById("output") as HTMLParagraphElement;

equalButton.addEventListener("click", function () {
  const value = +inputNumber.value;
  const result = value === 10;
  output.innerText = result + "";
});
notEqualButton.addEventListener("click", function () {
  const value = +inputNumber.value;
  const result = value !== 10;
  output.innerText = result + "";
});
moreThanButton.addEventListener("click", function () {
  const value = +inputNumber.value;
  const result = value > 10;
  output.innerText = result + "";
});
moreThanOrEqualButton.addEventListener("click", function () {
  const value = +inputNumber.value;
  const result = value >= 10;
  output.innerText = result + "";
});

export {};