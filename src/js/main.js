// const divApp = document.getElementById('app')
// divApp.innerText = "Hello JavaScript"

const inputNumber = document.getElementById('inputNumber')
const equalButton = document.getElementById('equalButton')
const notEqualButton = document.getElementById('notEqualButton')
const moreThanButton = document.getElementById('moreThanButton')
const moreThanOrEqualButton = document.getElementById('moreThanOrEqualButton')
const output = document.getElementById('output')

equalButton.addEventListener('click', function () {
    let value = +inputNumber.value
    let result = (value === 10)
    output.innerText = result
})
notEqualButton.addEventListener('click', function () {
    let value = +inputNumber.value
    let result = (value !== 10)
    output.innerText = result
})
moreThanButton.addEventListener('click', function () {
    let value = +inputNumber.value
    let result = (value > 10)
    output.innerText = result + ''
})
moreThanOrEqualButton.addEventListener('click', function () {
    let value = +inputNumber.value
    let result = (value >= 10)
    output.innerText = result + ''
})
